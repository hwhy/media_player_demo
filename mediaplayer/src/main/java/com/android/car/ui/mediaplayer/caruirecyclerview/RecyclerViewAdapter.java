package com.android.car.ui.mediaplayer.caruirecyclerview;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.car.ui.mediaplayer.R;
import com.android.car.ui.mediaplayer.model.SongItem;

import java.util.ArrayList;
import java.util.HashMap;


public class RecyclerViewAdapter extends
        RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder> {
    private ArrayList<SongItem> mData;
    private OnSongListener mSongListener;
    private HashMap<String, Bitmap> bitmaps;

    public RecyclerViewAdapter(ArrayList<SongItem> data, OnSongListener songListener, HashMap<String, Bitmap> bitmaps) {
        this.mData = data;
        this.mSongListener = songListener;
        this.bitmaps = bitmaps;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.art_view_list_item, parent, false);
        return new RecyclerViewHolder(view, mSongListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        SongItem item = mData.get(position);
        if ( item.getImageid() != 0 ) {
            holder.mArtImageView.setImageResource(item.getImageid());
        } else if ( item.getImageUrl() != null ) {
            Bitmap bitmap = this.bitmaps.get(item.getImageUrl());
            holder.mArtImageView.setImageBitmap(bitmap);
        }
        holder.mTitleText.setText(item.getTitle());
        holder.mSubTitleText.setText(item.getSubTitle());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void updateData(ArrayList<SongItem> data) {
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }

    /**
     * Holds views for each element in the list.
     */
    public static class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView mArtImageView;
        TextView mTitleText;
        TextView mSubTitleText;
        OnSongListener songListener;

        RecyclerViewHolder(@NonNull View itemView, @NonNull OnSongListener songListener) {
            super(itemView);
            mTitleText = itemView.findViewById(R.id.titleTextView);
            mSubTitleText = itemView.findViewById(R.id.subTitleTextView);
            mArtImageView = itemView.findViewById(R.id.artListitemImageView);
            this.songListener = songListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            songListener.onSongClicked(getAdapterPosition());
        }
    }

    public interface OnSongListener {
        void onSongClicked(int position);
        void onBitmapLoaded(String url);
    }
}

