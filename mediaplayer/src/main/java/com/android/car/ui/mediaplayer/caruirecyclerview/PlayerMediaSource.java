package com.android.car.ui.mediaplayer.caruirecyclerview;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.media.MediaPlayer;
import android.util.Log;
import com.android.car.ui.mediaplayer.R;
import com.android.car.ui.mediaplayer.model.SongItem;
import android.webkit.WebView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.android.car.ui.mediaplayer.R;
import com.android.car.ui.mediaplayer.model.SongItem;
import com.android.car.ui.toolbar.MenuItem;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.error.ANError;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.spotify.android.appremote.api.ConnectionParams;
import com.spotify.android.appremote.api.Connector;
import com.spotify.android.appremote.api.SpotifyAppRemote;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.specification.Album;
import com.wrapper.spotify.model_objects.specification.Image;
import com.wrapper.spotify.model_objects.specification.Paging;
import com.wrapper.spotify.model_objects.specification.Playlist;
import com.wrapper.spotify.model_objects.specification.PlaylistTrack;
import com.wrapper.spotify.model_objects.specification.Track;
import com.wrapper.spotify.model_objects.specification.TrackSimplified;
import com.wrapper.spotify.requests.data.albums.GetAlbumRequest;
import com.wrapper.spotify.requests.data.albums.GetAlbumsTracksRequest;
import com.wrapper.spotify.requests.data.library.SaveTracksForUserRequest;
import com.wrapper.spotify.requests.data.playlists.GetPlaylistCoverImageRequest;
import com.wrapper.spotify.requests.data.playlists.GetPlaylistsItemsRequest;
import com.wrapper.spotify.requests.data.tracks.GetTrackRequest;

import org.apache.hc.core5.http.Header;
import org.apache.hc.core5.http.ParseException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

public class PlayerMediaSource {
    private static final int REQUEST_CODE = 1337;
    private static final String CLIENT_ID = "a9d39ce795264738bf6a45df82147e79";
    private static final String REDIRECT_URI = "com.android.car.ui.mediaplayer:/oauth2redirect";
    private String spotifyAccessToken = "";
    private static final String albumId = "5zT1JLIj9E57p3e1rFm9Uq";
    private MediaPlayer mediaPlayer = null;
    private SpotifyApi spotifyApi = null;
    public static HashMap<String, Bitmap> bitmaps = new HashMap<String, Bitmap>();
    RecyclerViewAdapter.OnSongListener songListener = null;

    private SpotifyAppRemote mSpotifyAppRemote;

    public void acquireBitmap(String url) {
        Picasso.get().load(url).into(new Target(){
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                bitmaps.put(url, bitmap);
                if (songListener != null)
                    songListener.onBitmapLoaded(url);
//                Drawable drawable = new BitmapDrawable(activity.getResources(), bitmap);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
            }

            @Override
            public void onPrepareLoad(final Drawable placeHolderDrawable) {
            }
        });
    }

    public void readFromDevice() {
//        val singleFile = MediaStoreCompat.fromRelativePath(this, Environment.DIRECTORY_DOWNLOADS + "/MyApp/SubDir/", "file.txt")
    }

    public void readFromUsb(Activity activity) throws IOException {
        UsbManager manager = (UsbManager) activity.getSystemService(Context.USB_SERVICE);
        List<UsbSerialDriver> availableDrivers = UsbSerialProber.getDefaultProber().findAllDrivers(manager);
        if (availableDrivers.isEmpty()) {
            return;
        }

        // Open a connection to the first available driver.
        UsbSerialDriver driver = availableDrivers.get(0);
        UsbDeviceConnection connection = manager.openDevice(driver.getDevice());
        if (connection == null) {
            // add UsbManager.requestPermission(driver.getDevice(), ..) handling here
            return;
        }

        UsbSerialPort port = driver.getPorts().get(0); // Most devices have just one port (port 0)
        port.open(connection);
        port.setParameters(115200, 8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE);
    }

    public void requestAuthorization(Activity activity) {
        AuthenticationRequest authRequest =
                new AuthenticationRequest.Builder(CLIENT_ID, AuthenticationResponse.Type.TOKEN, REDIRECT_URI)
                        .setScopes(new String[]{"streaming", "playlist-read-private", "user-read-email", "user-read-private", "user-library-modify", "user-modify-playback-state"})
                        .build();

        AuthenticationClient.openLoginActivity(activity, REQUEST_CODE, authRequest);
//        AuthenticationClient.openLoginInBrowser(this, authRequest);

//        ConnectionParams connectionParams =
//                new ConnectionParams.Builder(CLIENT_ID)
//                        .setRedirectUri(REDIRECT_URI)
//                        .setAuthMethod(ConnectionParams.AuthMethod.APP_ID)
//                        .build();
//
//        SpotifyAppRemote.connect(activity, connectionParams,
//                new Connector.ConnectionListener() {
//
//                    @Override
//                    public void onConnected(SpotifyAppRemote spotifyAppRemote) {
//                        mSpotifyAppRemote = spotifyAppRemote;
//                        Log.d("MainActivity", "Connected! Yay!");
//
//                        // Now you can start interacting with App Remote
////                        connected();
//                    }
//
//                    @Override
//                    public void onFailure(Throwable throwable) {
//                        Log.e("MainActivity", throwable.getMessage(), throwable);
//
//                        // Something went wrong when attempting to connect! Handle errors here
//                    }
//                });
    }

    public void responseAuthorization(int requestCode, int resultCode, Intent intent) {
        // Check if result comes from the correct activity
        if (requestCode == REQUEST_CODE ) {
            AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, intent);

            switch (response.getType()) {
                // Response was successful and contains auth token
                case TOKEN: {
                    spotifyAccessToken = response.getAccessToken();

                    if (spotifyApi == null)
                    {
                        spotifyApi = new SpotifyApi.Builder()
                                .setAccessToken(spotifyAccessToken)
                                .build();
                    }
                    // Handle successful response
                    break;
                }

                // Auth flow returned an error
                case ERROR:
                    // Handle error response
                    break;

                // Most likely auth flow was cancelled
                default:
                    // Handle other cases
            }
        }
    }

    public void fillDataFromSpotifyPlaylist(ArrayList<SongItem> data, GridCarUiRecyclerViewActivity activity) {
        songListener = activity;
        if (spotifyApi != null) {
            GetPlaylistsItemsRequest playlistItemsRequest = spotifyApi.getPlaylistsItems("37i9dQZF1DX0s5kDXi1oC5").build();
            CompletableFuture<Paging<PlaylistTrack>> playlistTracksFuture = playlistItemsRequest.executeAsync();
            Paging<PlaylistTrack> playlistTracks = playlistTracksFuture.join();

            for (PlaylistTrack playlistTrack : playlistTracks.getItems()) {
                Track track = (Track)playlistTrack.getTrack();
                Image image = track.getAlbum().getImages()[2];
                acquireBitmap(image.getUrl());
                data.add(new SongItem(track.getName(), track.getArtists()[0].getName(), "path", image.getUrl()));
            }
        }
    }

    public void fillDataFromSpotify(ArrayList<SongItem> data, GridCarUiRecyclerViewActivity activity) {
        if (spotifyApi != null) {
            fillDataFromSpotifyPlaylist(data, activity);
//            GetAlbumsTracksRequest albumsTracksRequest = spotifyApi.getAlbumsTracks(albumId).build();
//            final CompletableFuture<Paging<TrackSimplified>> pagingFuture = albumsTracksRequest.executeAsync();
//            final Paging<TrackSimplified> trackSimplifiedPaging = pagingFuture.join();
//
//            for (TrackSimplified trackSimplified : trackSimplifiedPaging.getItems()) {
//                Track track = spotifyApi.getTrack(trackSimplified.getId()).build().executeAsync().join();
//                Image image = track.getAlbum().getImages()[0];
//                acquireBitmap(image.getUrl());
//
//                data.add(new SongItem(trackSimplified.getName(), trackSimplified.getArtists()[0].getName(), "path", image.getUrl()));
//            }
        }
    }

    public void readFromSpotify() {
        try {
            // Thread free to do other tasks...
//            TreeMap<String,String> additionalHeaders = new TreeMap<String,String>();
//                    additionalHeaders.put("Authorization", "Bearer " + spotifyAccessToken);
//                    webView.loadUrl("https://open.spotify.com/embed?uri=spotify:album:5zT1JLIj9E57p3e1rFm9Uq", additionalHeaders);
//            webView.loadData("<!DOCTYPE html>\n" +
//                    "<html>\n" +
//                    "<head>\n" +
//                    "  <title>Spotify Web Playback SDK Quick Start Tutorial</title>\n" +
//                    "</head>\n" +
//                    "<body>\n" +
//                    "  <iframe src=\"https://open.spotify.com/embed?uri=spotify:album:5zT1JLIj9E57p3e1rFm9Uq\" allowtransparency=\"true\" width=\"300\" height=\"380\" frameborder=\"0\"></iframe>\n" +
//                    "</body>\n" +
//                    "</html>", ".html", "UTF-8");

        } catch (CompletionException e) {
            System.out.println("Error: " + e.getCause().getMessage());
        } catch (CancellationException e) {
            System.out.println("Async operation cancelled.");
        }
    }

    public void playSound(Activity activity, int soundId) throws PackageManager.NameNotFoundException, IOException {
        mediaPlayer = MediaPlayer.create(activity, soundId);
       // mediaPlayer.start();
//        String folderName = "cache";
//        File externalStorageCache = new File(activity.getExternalFilesDir(null) + File.separator + folderName);
//        if (!externalStorageCache.exists())
//            externalStorageCache.mkdirs();
//        String s1 = "/mnt/user/0/primary/Android/data/com.android.car.ui.mediaplayer/cache/.mp3";
//        File ff = new File(s1);
//        boolean readable = ff.canRead();
//        Uri myUri = Uri.parse(s1); // initialize Uri here
//        MediaPlayer mediaPlayer = new MediaPlayer();
//        mediaPlayer.setAudioAttributes(
//                new AudioAttributes.Builder()
//                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
//                        .setUsage(AudioAttributes.USAGE_MEDIA)
//                        .build()
//        );
//        mediaPlayer.setDataSource(activity.getApplicationContext(), myUri);
//        mediaPlayer.prepare();
//        mediaPlayer.start();
    }
}
