package com.android.car.ui.mediaplayer.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SongItem implements Parcelable {

    private String title;
    private String subTitle;
    private String path;
    private int imageid = 0;
    private String imageUrl;

    protected SongItem(Parcel in) {
        title = in.readString();
        subTitle = in.readString();
        path = in.readString();
        imageid = in.readInt();
        imageUrl = in.readString();
    }

    public static final Creator<SongItem> CREATOR = new Creator<SongItem>() {
        @Override
        public SongItem createFromParcel(Parcel in) {
            return new SongItem(in);
        }

        @Override
        public SongItem[] newArray(int size) {
            return new SongItem[size];
        }
    };

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public SongItem(String title, String subTitle, String path, int imageid) {
        this.title = title;
        this.subTitle = subTitle;
        this.path = path;
        this.imageid = imageid;
    }

    public SongItem(String title, String subTitle, String path, String imageUrl) {
        this.title = title;
        this.subTitle = subTitle;
        this.path = path;
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public int getImageid() {
        return imageid;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageid(int imageid) {
        this.imageid = imageid;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(subTitle);
        dest.writeString(path);
        dest.writeInt(imageid);
        dest.writeString(imageUrl);
    }
}
