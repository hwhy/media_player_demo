package com.android.car.ui.mediaplayer.caruirecyclerview;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.transition.TransitionInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.car.ui.baselayout.Insets;
import com.android.car.ui.baselayout.InsetsChangedListener;
import com.android.car.ui.core.CarUi;
import com.android.car.ui.mediaplayer.R;
import com.android.car.ui.mediaplayer.model.SongItem;
import com.android.car.ui.mediaplayer.preferences.PreferenceActivity;
import com.android.car.ui.recyclerview.CarUiRecyclerView;
import com.android.car.ui.toolbar.MenuItem;
import com.android.car.ui.toolbar.TabLayout;
import com.android.car.ui.toolbar.Toolbar;
import com.android.car.ui.toolbar.ToolbarController;

import java.util.ArrayList;
import java.util.List;

public class PlayActivity extends AppCompatActivity implements
        InsetsChangedListener {
    private final ArrayList<String> mData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS);
        setContentView(R.layout.play_view_activity);
        setUpToolBar();
        setUpPlaybackControls();

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            SongItem item = extras.getParcelable("SongItem");
            ImageView backSong = findViewById(R.id.artListitemImageViewPlayBack);
            ImageView frontSong = findViewById(R.id.artListitemImageViewPlayFront);
            TextView title = findViewById(R.id.titleTextViewPlay);
            TextView subtitle = findViewById(R.id.subtitleTextViewPlay);
            if (item.getImageid() == 0) {
                Bitmap bitmap = PlayerMediaSource.bitmaps.get(item.getImageUrl());
                backSong.setImageBitmap(bitmap);
                frontSong.setImageBitmap(bitmap);
            } else {
                backSong.setImageResource(item.getImageid());
                frontSong.setImageResource(item.getImageid());
            }
            title.setText(item.getTitle());
            subtitle.setText(item.getSubTitle());
        }
    }

    private void setUpPlaybackControls() {
        SeekBar progressbar = findViewById(R.id.progressBar);
        progressbar.setThumbTintList(ColorStateList.valueOf(Color.GRAY));
    }
    private void setUpToolBar() {
        ToolbarController toolbar = getToolbar();
        toolbar.setLogo(R.drawable.ic_launcher);
        toolbar.setState(Toolbar.State.SUBPAGE);
        toolbar.setNavButtonMode(Toolbar.NavButtonMode.DOWN);
        toolbar.setTitle(getTitle());
    }

    private ToolbarController getToolbar() {
        ToolbarController toolbar = CarUi.getToolbar(this);
        if (toolbar == null) {
            toolbar = requireViewById(R.id.toolbar);
        }
        return toolbar;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //overridePendingTransition(R.anim.slide_out_bottom, R.anim.slide_out_bottom);
    }

    @Override
    public void onCarUiInsetsChanged(@NonNull Insets insets) {
        requireViewById(android.R.id.content)
                .setPadding(insets.getLeft(), 0, insets.getRight(), 0);
    }
}
