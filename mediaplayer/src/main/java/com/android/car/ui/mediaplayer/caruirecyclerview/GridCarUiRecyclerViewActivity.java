package com.android.car.ui.mediaplayer.caruirecyclerview;

import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.car.ui.AlertDialogBuilder;
import com.android.car.ui.baselayout.Insets;
import com.android.car.ui.baselayout.InsetsChangedListener;
import com.android.car.ui.core.CarUi;
import com.android.car.ui.mediaplayer.R;
import com.android.car.ui.mediaplayer.broadcastreceiver.SpotifyBroadcastReceiver;
import com.android.car.ui.mediaplayer.model.SongItem;
import com.android.car.ui.mediaplayer.preferences.PreferenceActivity;
import com.android.car.ui.recyclerview.CarUiContentListItem;
import com.android.car.ui.recyclerview.CarUiRadioButtonListItem;
import com.android.car.ui.recyclerview.CarUiRadioButtonListItemAdapter;
import com.android.car.ui.recyclerview.CarUiRecyclerView;
import com.android.car.ui.toolbar.MenuItem;
import com.android.car.ui.toolbar.TabLayout;
import com.android.car.ui.toolbar.Toolbar;
import com.android.car.ui.toolbar.ToolbarController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.spotify.sdk.android.authentication.AuthenticationResponse;

public class GridCarUiRecyclerViewActivity extends AppCompatActivity implements
        InsetsChangedListener, RecyclerViewAdapter.OnSongListener {
    PlayerMediaSource playerMediaSource = new PlayerMediaSource();
    private final ArrayList<SongItem> mData = new ArrayList<>();
    CarUiRecyclerView recyclerView;
    RecyclerViewAdapter adapter;
    AlertDialog dialog;
    private int activeSource = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grid_car_ui_recycler_view_activity);
        setUpToolBar();
        recyclerView = findViewById(R.id.list);
        final int recyclerViewItemMargin = 15;
        recyclerView.addItemDecoration(new SimpleMarginItemDecoration(recyclerViewItemMargin));
        adapter = new RecyclerViewAdapter(generateSampleData(), this, playerMediaSource.bitmaps);
        recyclerView.setAdapter(adapter);

        showSourceData(activeSource);
    }

    @Override
    public void onStart() {
        super.onStart();

        playerMediaSource.requestAuthorization(this);

        try {
            playerMediaSource.playSound(this, R.raw.dorofeevagorit);
        } catch (PackageManager.NameNotFoundException | IOException e) {
            e.printStackTrace();
        }

        BroadcastReceiver br = new SpotifyBroadcastReceiver();
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        this.registerReceiver(br, filter);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Uri uri = intent.getData();
        if (uri != null) {
            AuthenticationResponse response = AuthenticationResponse.fromUri(uri);

            switch (response.getType()) {
                // Response was successful and contains auth token
                case TOKEN:
                    // Handle successful response
                    break;

                // Auth flow returned an error
                case ERROR:
                    // Handle error response
                    break;

                // Most likely auth flow was cancelled
                default:
                    // Handle other cases
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        playerMediaSource.responseAuthorization(requestCode, resultCode, intent);
    }
    
    private void setUpToolBar() {
        ToolbarController toolbar = getToolbar();
        toolbar.setState(Toolbar.State.HOME);
        toolbar.setTitle(getTitle());
        toolbar.setLogo(R.drawable.ic_launcher);
        toolbar.registerOnBackListener(
                () -> {
                    if (toolbar.getState() == Toolbar.State.SEARCH
                            || toolbar.getState() == Toolbar.State.EDIT) {
                        toolbar.setState(Toolbar.State.HOME);
                        CarUiRecyclerView recyclerView = findViewById(R.id.list);
                        recyclerView.setVisibility(View.VISIBLE);
                        return true;
                    }
                    return false;
                });
        setUpToolBarTabs();
        setUpToolBarMenuButtons();
    }

    private  void setUpToolBarTabs() {
        ToolbarController toolbar = getToolbar();
        String sourceName = (activeSource == 0 ? "USB 3.0..." : (activeSource == 1 ? "Local ..." : "Spotify"));
        toolbar.addTab(new TabLayout.Tab(getDrawable(R.drawable.ic_input), sourceName));
        toolbar.addTab(new TabLayout.Tab(getDrawable(R.drawable.ic_recent), "Recent"));
        toolbar.addTab(new TabLayout.Tab(getDrawable(R.drawable.ic_library), "Library"));
        toolbar.addTab(new TabLayout.Tab(getDrawable(R.drawable.ic_fav), "Favourites"));
        toolbar.registerOnTabSelectedListener(new Toolbar.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int tabPos = toolbar.getTabLayout().getTabPosition(tab);
                if (tabPos == 0){
//                    showSelectSourceDialog();
//                    toolbar.selectTab(1);
                }
            }
        });
    }

    private  void setUpToolBarMenuButtons() {
        final ToolbarController toolbar = getToolbar();
        final List<MenuItem> mMenuItems = new ArrayList<>();
        mMenuItems.add(MenuItem.builder(this)
                .setToSearch()
                .setOnClickListener(i ->  {
                        toolbar.setState(Toolbar.State.SEARCH);
                        CarUiRecyclerView recyclerView = findViewById(R.id.list);
                        recyclerView.setVisibility(View.GONE);
                })
                .build());
        mMenuItems.add(MenuItem.builder(this)
                .setToSettings()
                .setOnClickListener(i ->
                        startActivity(new Intent(this, PreferenceActivity.class), ActivityOptions.makeSceneTransitionAnimation(this).toBundle()))
                .build());
        toolbar.setMenuItems(mMenuItems);
    }

    private ToolbarController getToolbar() {
        ToolbarController toolbar = CarUi.getToolbar(this);
        if (toolbar == null) {
            toolbar = requireViewById(R.id.toolbar);
        }
        return toolbar;
    }

    private void showSelectSourceDialog() {
        ArrayList<CarUiRadioButtonListItem> data = new ArrayList<>();

        CarUiRadioButtonListItem item = new CarUiRadioButtonListItem();
        item.setTitle("USB");
        item.setOnCheckedChangeListener(new CarUiContentListItem.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(@NonNull CarUiContentListItem item, boolean isChecked) {
                if (isChecked) {
                    showSourceData(0);
                    dismissAlertView();
                }
            }
        });
        data.add(item);

        item = new CarUiRadioButtonListItem();
        item.setTitle("Local storage");
        item.setChecked(true);
        item.setOnCheckedChangeListener(new CarUiContentListItem.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(@NonNull CarUiContentListItem item, boolean isChecked) {
                if (isChecked) {
                    showSourceData(1);
                    dismissAlertView();
                }
            }
        });
        data.add(item);

        item = new CarUiRadioButtonListItem();
        item.setTitle("Spotify");
        item.setOnCheckedChangeListener(new CarUiContentListItem.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(@NonNull CarUiContentListItem item, boolean isChecked) {
                if (isChecked) {
                    //TODO: Load spotify
                    showSourceData(2);
                    dismissAlertView();
                }
            }
        });
        data.add(item);

        dialog = new AlertDialogBuilder(this)
                .setTitle("Select media source")
                .setSingleChoiceItems(new CarUiRadioButtonListItemAdapter(data))
                .setPositiveButton("OK", (dialogInterface, i) -> {
                })
                .show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setVisibility(View.GONE);
    }

    private void showSourceData(int sourceIndex) {
        mData.clear();
        this.activeSource = sourceIndex;

        switch (sourceIndex) {
            case 0:
                Toast.makeText(this, "USB selected", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                generateSampleData();
                //Toast.makeText(this, "Local storage selected", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                //playerMediaSource.fillDataFromSpotify(mData, this);
                //Toast.makeText(this, "Spotify selected", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }

//        ToolbarController toolbar = getToolbar();
//        toolbar.clearAllTabs();
//        setUpToolBarTabs();
//        setUpToolBarMenuButtons();

        //adapter.updateData(mData);
    }

    private void dismissAlertView() {
        if (dialog != null) {
            final Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    dialog.dismiss();
                }
            }, 500);
        }
    }
    private ArrayList<SongItem> generateSampleData() {
        mData.add( new SongItem("No tears left to cry", "Ariana Grande", "path", R.drawable.song1));
        mData.add( new SongItem("Enemy", "Magdalena", "path", R.drawable.song2));
        mData.add( new SongItem("Starboy", "The weekend", "path", R.drawable.song3));
        mData.add( new SongItem("Pain", "Ryan Jones", "path", R.drawable.song4));
        mData.add( new SongItem("Plastic Hearts", "Miley Cyrus", "path", R.drawable.song5));
        mData.add( new SongItem("I am you", "Stray Kids", "path", R.drawable.song6));
        mData.add( new SongItem("Blaze away", "Morcheeba", "path", R.drawable.song7));
        mData.add( new SongItem("Skye & Ross", "Skye & Ross", "path", R.drawable.song8));
        mData.add( new SongItem("The Antidote", "Morcheeba", "path", R.drawable.song9));
        return mData;
    }

    @Override
    public void onCarUiInsetsChanged(@NonNull Insets insets) {
        requireViewById(R.id.list)
                .setPadding(0, insets.getTop(), 0, insets.getBottom());
        requireViewById(android.R.id.content)
                .setPadding(insets.getLeft(), 0, insets.getRight(), 0);
    }

    @Override
    public void onSongClicked(int position) {
        SongItem item = mData.get(position);
        Intent intent = new Intent(this,
                com.android.car.ui.mediaplayer.caruirecyclerview.PlayActivity.class);
        intent.putExtra("SongItem", item);
        startActivity( intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
//        BlankFragment myFragment = new BlankFragment();
//        getSupportFragmentManager()
//                .beginTransaction().setCustomAnimations(
//                android.R.anim.slide_out_right,
//                android.R.anim.slide_in_left,
//                android.R.anim.slide_out_right,
//                android.R.anim.slide_in_left)
//                .add(R.id.frame_inner, myFragment, null).addToBackStack("name")
//                .commit();

//        FragmentManager fragmentManager = getSupportFragmentManager();
//        fragmentManager.beginTransaction()
//                .replace(R.id.fragment_container, ExampleFragment.class, null)
//                .setReorderingAllowed(true)
//                .addToBackStack("name") // name can be null
//                .commit();
    }

    @Override
    public void onBitmapLoaded(String url) {
        recyclerView.forceLayout();
    }

    public static class SimpleMarginItemDecoration extends RecyclerView.ItemDecoration {
        private final int margin;
        public SimpleMarginItemDecoration(int margin) {
            this.margin = margin;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            outRect.left = margin;
            outRect.right = margin;
            outRect.top = margin;
            outRect.bottom = margin;
        }
    }
}
